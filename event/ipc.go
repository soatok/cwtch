package event

// IPCMessage is a wrapper for a regular eventMessage with a destination (onion|AppDest) so the other side of the bridge can route appropriately
type IPCMessage struct {
	Dest    string
	Message Event
}

// IPCBridge is an interface to a IPC construct used to communicate IPCMessages
type IPCBridge interface {
	Read() (*IPCMessage, bool)
	Write(message *IPCMessage)
	Shutdown()
}
