package bridge

import (
	"cwtch.im/cwtch/event"
	"git.openprivacy.ca/openprivacy/log"
	"os"
	"testing"
	"time"
)

var (
	clientPipe  = "./client"
	servicePipe = "./service"
)

func clientHelper(t *testing.T, in, out string, messageOrig *event.IPCMessage, done chan bool) {
	client := NewPipeBridgeClient(in, out)

	messageAfter, ok := client.Read()
	if !ok {
		t.Errorf("Reading from client IPCBridge failed")
		done <- true
		return
	}

	if messageOrig.Dest != messageAfter.Dest {
		t.Errorf("Dest's value differs expected: %v actaul: %v", messageOrig.Dest, messageAfter.Dest)
	}

	if messageOrig.Message.EventType != messageAfter.Message.EventType {
		t.Errorf("EventTypes's value differs expected: %v actaul: %v", messageOrig.Message.EventType, messageAfter.Message.EventType)
	}

	if messageOrig.Message.Data[event.Identity] != messageAfter.Message.Data[event.Identity] {
		t.Errorf("Data[Identity]'s value differs expected: %v actaul: %v", messageOrig.Message.Data[event.Identity], messageAfter.Message.Data[event.Identity])
	}

	done <- true
}

func serviceHelper(t *testing.T, in, out string, messageOrig *event.IPCMessage, done chan bool) {
	service := NewPipeBridgeService(in, out)

	service.Write(messageOrig)

	done <- true
}

func TestPipeBridge(t *testing.T) {
	os.Remove(servicePipe)
	os.Remove(clientPipe)

	messageOrig := &event.IPCMessage{Dest: "ABC", Message: event.NewEventList(event.NewPeer, event.Identity, "It is I")}
	serviceDone := make(chan bool)
	clientDone := make(chan bool)

	go clientHelper(t, clientPipe, servicePipe, messageOrig, clientDone)
	go serviceHelper(t, servicePipe, clientPipe, messageOrig, serviceDone)

	<-serviceDone
	<-clientDone
}

func restartingClient(t *testing.T, in, out string, done chan bool) {
	client := NewPipeBridgeClient(in, out)

	message1 := &event.IPCMessage{Dest: "ABC", Message: event.NewEventList(event.NewPeer)}
	log.Infoln("client writing message 1")
	client.Write(message1)

	time.Sleep(100 * time.Millisecond)
	log.Infoln("client shutdown")
	client.Shutdown()

	log.Infoln("client new client")
	client = NewPipeBridgeClient(in, out)
	message2 := &event.IPCMessage{Dest: "ABC", Message: event.NewEventList(event.DeleteContact)}
	log.Infoln("client2 write message2")
	client.Write(message2)

	done <- true
}

func stableService(t *testing.T, in, out string, done chan bool) {
	service := NewPipeBridgeService(in, out)

	log.Infoln("service wait read 1")
	message1, ok := service.Read()
	log.Infof("service read 1 %v ok:%v\n", message1, ok)
	if !ok {
		t.Errorf("Reading from client IPCBridge 1st time failed")
		done <- true
		return
	}
	if message1.Message.EventType != event.NewPeer {
		t.Errorf("Wrong message received, expected NewPeer\n")
		done <- true
		return
	}

	log.Infoln("service wait read 2")
	message2, ok := service.Read()
	log.Infof("service read 2 got %v ok:%v\n", message2, ok)
	if !ok {
		t.Errorf("Reading from client IPCBridge 2nd time failed")
		done <- true
		return
	}
	if message2.Message.EventType != event.DeleteContact {
		t.Errorf("Wrong message received, expected DeleteContact, got %v\n", message2)
		done <- true
		return
	}

	done <- true
}

func TestReconnect(t *testing.T) {
	log.Infoln("TestReconnect")
	os.Remove(servicePipe)
	os.Remove(clientPipe)

	serviceDone := make(chan bool)
	clientDone := make(chan bool)

	go restartingClient(t, clientPipe, servicePipe, clientDone)
	go stableService(t, servicePipe, clientPipe, serviceDone)

	<-serviceDone
	<-clientDone
}
