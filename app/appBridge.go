package app

import "cwtch.im/cwtch/event"
import "git.openprivacy.ca/openprivacy/log"

const (
	// DestApp should be used as a destination for IPC messages that are for the application itself an not a peer
	DestApp = "app"
)

type applicationBridge struct {
	applicationCore

	bridge event.IPCBridge
	handle func(*event.Event)
}

func (ab *applicationBridge) listen() {
	log.Infoln("ab.listen()")
	for {
		ipcMessage, ok := ab.bridge.Read()
		log.Debugf("listen() got %v for %v\n", ipcMessage.Message.EventType, ipcMessage.Dest)
		if !ok {
			log.Debugln("exiting appBridge.listen()")
			return
		}

		if ipcMessage.Dest == DestApp {
			ab.handle(&ipcMessage.Message)
		} else {
			if eventBus, exists := ab.eventBuses[ipcMessage.Dest]; exists {
				eventBus.PublishLocal(ipcMessage.Message)
			}
		}
	}
}

func (ab *applicationBridge) Shutdown() {
}
