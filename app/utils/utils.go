package utils

import (
	app2 "cwtch.im/cwtch/app"
	"cwtch.im/cwtch/model/attr"
	"cwtch.im/cwtch/peer"
	"time"
)

// WaitGetPeer is a helper function for utility apps not written using the event bus
// Proper use of an App is to call CreatePeer and then process the NewPeer event
// however for small utility use, this function which polls the app until the peer is created
// may fill that usecase better
func WaitGetPeer(app app2.Application, name string) peer.CwtchPeer {
	for {
		for id := range app.ListPeers() {
			peer := app.GetPeer(id)
			localName, _ := peer.GetAttribute(attr.GetLocalScope("name"))
			if localName == name {
				return peer
			}
		}
		time.Sleep(100 * time.Millisecond)
	}
}
