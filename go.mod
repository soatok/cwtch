module cwtch.im/cwtch

go 1.14

require (
	git.openprivacy.ca/cwtch.im/tapir v0.4.3
	git.openprivacy.ca/openprivacy/connectivity v1.4.4
	git.openprivacy.ca/openprivacy/log v1.0.2
	github.com/gtank/ristretto255 v0.1.2
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/struCoder/pidusage v0.1.3
	golang.org/x/crypto v0.0.0-20201012173705-84dcc777aaee
	golang.org/x/lint v0.0.0-20210508222113-6edffad5e616 // indirect
	golang.org/x/net v0.0.0-20210405180319-a5a99cb37ef4
	golang.org/x/tools v0.1.2 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
)
